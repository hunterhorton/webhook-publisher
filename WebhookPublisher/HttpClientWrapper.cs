﻿using System.Net.Http;
using System.Threading.Tasks;

namespace WebhookPublisher
{
    public class HttpClientWrapper
    {
        private static readonly HttpClient HttpClient = new HttpClient();

        public virtual async Task<HttpResponseMessage> SendAsync(HttpRequestMessage message)
        {
            return await HttpClient.SendAsync(message);
        }
    }
}