﻿using System;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using Microsoft.Azure.EventHubs;
using Microsoft.Azure.EventHubs.Processor;
using WebhookPublisher.Config;
using WebhookPublisher.Logging;
using WebhookPublisher.Runners;

namespace WebhookPublisher
{
    public class Program
    {
        private static ILog _log;
        private static readonly WebhookRetryRunner RetryRunner = new WebhookRetryRunner();

        public static void Main(string[] args)
        {
            LoggerFactory.SetupLogger();
            _log = LoggerFactory.GetLogger();
            MainAsync(args).GetAwaiter().GetResult();
        }

        private static async Task MainAsync(string[] args)
        {
            var config = new ConfigurationProvider();
            var eventProcessorHost = new EventProcessorHost(
                config.GetEntityPath(),
                PartitionReceiver.DefaultConsumerGroupName,
                config.GetEventHubConnectionString(),
                config.GetDatabaseConnectionString(),
                config.GetStorageContainerName());

            var options = new EventProcessorOptions { MaxBatchSize = config.GetBatchSize() };
            // Registers the Event Processor Host and starts receiving messages
            await eventProcessorHost.RegisterEventProcessorAsync<WebhookEventProcessor>(options);

            var tokenSource = new CancellationTokenSource();
            //Start retryng failed posts
            var retryTask = RetryRunner.Run(tokenSource.Token);

            Console.WriteLine("Type \"y\" to stop listening");
            var input = "";
            while (input != null && input.ToLower() != "y")
            {
                input = Console.ReadLine();
            }

            // Disposes of the Event Processor Host
            await eventProcessorHost.UnregisterEventProcessorAsync();

            try
            {
                //Stop retrying failed posts
                tokenSource.Cancel();
                retryTask.Wait(tokenSource.Token);
            }
            //upon cancellation, thread throws exception
            catch (OperationCanceledException)
            {
            }
        }
    }
}