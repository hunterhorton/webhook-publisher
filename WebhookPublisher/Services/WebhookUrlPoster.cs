﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WebhookPublisher.Domain;

namespace WebhookPublisher.Services
{
    public class WebhookUrlPoster
    {
        private readonly HttpClientWrapper _httpClientWrapper;

        public WebhookUrlPoster() : this(new HttpClientWrapper())
        {
        }

        public WebhookUrlPoster(HttpClientWrapper httpClientWrapper)
        {
            _httpClientWrapper = httpClientWrapper;
        }

        public virtual async Task PostAsync(WebhookEvent webhookEvent, string url)
        {
            var message = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                Content = new StringContent(JsonConvert.SerializeObject(webhookEvent), Encoding.UTF8,
                    "application/json"),
                RequestUri = new Uri(url)
            };
            var result = await _httpClientWrapper.SendAsync(message);

            result.EnsureSuccessStatusCode();
        }
    }
}