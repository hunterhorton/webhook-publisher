﻿using System.Net.Http;
using System.Threading.Tasks;
using log4net;
using Newtonsoft.Json;
using WebhookPublisher.Config;
using WebhookPublisher.Domain;
using WebhookPublisher.Logging;
using WebhookPublisher.Mappings;
using WebhookPublisher.Repositories;

namespace WebhookPublisher.Services
{
    public class WebhookRetryService
    {
        private readonly FailedEventsRepository _repository;
        private readonly WebhookUrlPoster _poster;
        private readonly IMapper<FailedEvent, WebhookEvent> _mapper;
        private readonly ConfigurationProvider _configurationProvider;
        private readonly ILog _log;

        public WebhookRetryService() : this(new FailedEventsRepository(), new WebhookUrlPoster(),
            new FailedEventToWebhookEventMapper(), new ConfigurationProvider())
        {
        }

        public WebhookRetryService(FailedEventsRepository repository, WebhookUrlPoster poster,
            IMapper<FailedEvent, WebhookEvent> mapper, ConfigurationProvider configurationProvider)
        {
            _repository = repository;
            _poster = poster;
            _mapper = mapper;
            _configurationProvider = configurationProvider;
            _log = LoggerFactory.GetLogger();
        }

        public virtual async Task Retry()
        {
            var events = await _repository.GetFailedEvents();
            var postTasks = new Task[events.Count];
            var index = 0;
            foreach (var failedEvent in events)
            {
                postTasks[index] = PostEvent(failedEvent);
                index++;
            }

            await Task.WhenAll(postTasks);
        }

        private async Task PostEvent(FailedEvent failedEvent)
        {
            var retryEvent = _mapper.Map(failedEvent);
            try
            {
                await _poster.PostAsync(retryEvent, failedEvent.Url);
                _log.Info($"Successful post: {failedEvent.Url}");
                await _repository.RemoveFailedEvent(failedEvent);
            }
            catch (HttpRequestException e)
            {
                failedEvent.RetryCount++;
                _log.Error(
                    $"Error encountered for url: {failedEvent.Url} | Exception: {e.GetBaseException().Message}");
                if (failedEvent.RetryCount >= _configurationProvider.GetRetryLimit())
                {
                    await _repository.RemoveFailedEvent(failedEvent);
                    _log.Error($"Event exceeds retry limit: {JsonConvert.SerializeObject(failedEvent)}");
                }
                else
                {
                    await _repository.UpdateRetryCount(failedEvent);
                }
            }
        }
    }
}