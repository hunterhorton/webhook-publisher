﻿using System.Net.Http;
using System.Threading.Tasks;
using log4net;
using WebhookPublisher.Domain;
using WebhookPublisher.Logging;
using WebhookPublisher.Mappings;
using WebhookPublisher.Queries;
using WebhookPublisher.Repositories;

namespace WebhookPublisher.Services
{
    public class WebhookSubscriptionService
    {
        private readonly WebhookUrlQuery _urlQuery;
        private readonly WebhookUrlPoster _webhookUrlPoster;
        private readonly FailedEventsRepository _failedEventsRepo;
        private readonly IMapper<JovixEvent, WebhookEvent> _webhookMapper;
        private readonly JovixEventToFailedEventMapper _failedEventMapper;
        private readonly ILog _log;

        public WebhookSubscriptionService() : this(new WebhookUrlQuery(), new WebhookUrlPoster(),
            new FailedEventsRepository(), new JovixEventToWebhookEventMapper(), new JovixEventToFailedEventMapper())
        {
        }

        public WebhookSubscriptionService(WebhookUrlQuery urlQuery, WebhookUrlPoster webhookUrlPoster,
            FailedEventsRepository failedEventsRepo, IMapper<JovixEvent, WebhookEvent> webhookMapper,
            JovixEventToFailedEventMapper failedEventMapper)
        {
            _urlQuery = urlQuery;
            _webhookUrlPoster = webhookUrlPoster;
            _failedEventsRepo = failedEventsRepo;
            _webhookMapper = webhookMapper;
            _failedEventMapper = failedEventMapper;
            _log = LoggerFactory.GetLogger();
        }

        public async Task PostEventAsync(JovixEvent jovixEvent)
        {
            var webhookEvent = _webhookMapper.Map(jovixEvent);
            var urls = await _urlQuery.GetUrls(jovixEvent.InstanceId, jovixEvent.ProjectId, jovixEvent.EntityType,
                jovixEvent.ChangeType);
            var postTasks = new Task[urls.Count];
            for (var index = 0; index < urls.Count; index++)
            {
                var url = urls[index];
                _log.Info($"Queing Post to: {url}");
                postTasks[index] = PostHelper(jovixEvent, webhookEvent, url);
            }

            Task.WaitAll(postTasks);
        }

        private async Task PostHelper(JovixEvent jovixEvent, WebhookEvent webhookEvent, string url)
        {
            try
            {
                await _webhookUrlPoster.PostAsync(webhookEvent, url);
                _log.Info($"Successful post: {url}");
            }
            catch (HttpRequestException ex)
            {
                var failedEvent = _failedEventMapper.Map(jovixEvent, url);
                await _failedEventsRepo.SaveEventAsync(failedEvent);
                _log.Error($"Error encountered for url: {url} | Exception: {ex.GetBaseException().Message}");
            }
        }
    }
}