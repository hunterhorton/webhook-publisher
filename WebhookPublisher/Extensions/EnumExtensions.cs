﻿using System;
using System.ComponentModel;

namespace WebhookPublisher.Extensions
{
    public static class EnumExtensions
    {
        public static string Name(this Enum value)
        {
            var fi = value.GetType().GetField(value.ToString());

            if (fi?.GetCustomAttributes(typeof(DescriptionAttribute), false) is DescriptionAttribute[] attributes &&
                attributes.Length > 0)
            {
                return attributes[0].Description ?? value.ToString();
            }

            return value.ToString();
        }
    }
}