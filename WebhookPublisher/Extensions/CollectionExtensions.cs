﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebhookPublisher.Extensions
{
    public static class CollectionExtensions
    {
        private static readonly Random _random = new Random();

        public static T Random<T>(this IEnumerable<T> collection)
        {
            var array = collection as T[] ?? collection.ToArray();

            var index = _random.Next() % array.Length;
            return array[index];
        }
    }
}