﻿using System.Threading;
using System.Threading.Tasks;
using WebhookPublisher.Config;
using WebhookPublisher.Services;

namespace WebhookPublisher.Runners
{
    public class WebhookRetryRunner
    {
        private readonly WebhookRetryService _retryService;
        private readonly ConfigurationProvider _configurationProvider;

        public WebhookRetryRunner() : this(new WebhookRetryService(), new ConfigurationProvider())
        {
        }

        public WebhookRetryRunner(WebhookRetryService retryService, ConfigurationProvider configurationProvider)
        {
            _retryService = retryService;
            _configurationProvider = configurationProvider;
        }

        public async Task Run(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                await _retryService.Retry();
                await Task.Delay(_configurationProvider.GetRetryDelay(), token);
            }
        }
    }
}