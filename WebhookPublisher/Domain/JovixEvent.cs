﻿using System;
using WebhookPublisher.Enums;

namespace WebhookPublisher.Domain
{
    public class JovixEvent
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public ChangeType ChangeType { get; set; }
        public DateTime Timestamp { get; set; }
        public EntityType EntityType { get; set; }
        public string[] Columns { get; set; }
        public long SequenceNumber { get; set; }
        public Guid InstanceId { get; set; }
    }
}