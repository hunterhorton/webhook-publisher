﻿using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace WebhookPublisher.Domain
{
    public class FailedEvent : TableEntity
    {
        public int EntityId { get; set; }
        public int ProjectId { get; set; }
        public int ChangeType { get; set; }
        public DateTime EventTime { get; set; }
        public int EntityType { get; set; }
        public string[] Columns { get; set; }
        public long SequenceNumber { get; set; }
        public string Url { get; set; }
        public int RetryCount { get; set; }
    }
}