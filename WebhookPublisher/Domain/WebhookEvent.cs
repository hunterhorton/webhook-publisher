﻿using System;

namespace WebhookPublisher.Domain
{
    public class WebhookEvent
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string ChangeType { get; set; }
        public DateTime EventTime { get; set; }
        public string EntityType { get; set; }
        public string[] Columns { get; set; }
        public long SequenceNumber { get; set; }
    }
}