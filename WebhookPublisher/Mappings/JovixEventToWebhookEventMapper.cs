﻿using WebhookPublisher.Domain;
using WebhookPublisher.Extensions;

namespace WebhookPublisher.Mappings
{
    public class JovixEventToWebhookEventMapper : IMapper<JovixEvent, WebhookEvent>
    {
        public virtual WebhookEvent Map(JovixEvent jovixEvent)
        {
            return new WebhookEvent
            {
                ChangeType = jovixEvent.ChangeType.Name(),
                EntityType = jovixEvent.EntityType.Name(),
                Id = jovixEvent.Id,
                ProjectId = jovixEvent.ProjectId,
                SequenceNumber = jovixEvent.SequenceNumber,
                EventTime = jovixEvent.Timestamp,
                Columns = jovixEvent.Columns
            };
        }
    }
}