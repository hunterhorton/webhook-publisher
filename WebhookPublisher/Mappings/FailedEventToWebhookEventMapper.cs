﻿using WebhookPublisher.Domain;
using WebhookPublisher.Enums;
using WebhookPublisher.Extensions;

namespace WebhookPublisher.Mappings
{
    public class FailedEventToWebhookEventMapper: IMapper<FailedEvent, WebhookEvent>
    {
        public WebhookEvent Map(FailedEvent source)
        {
            return new WebhookEvent
            {
                ChangeType = ((ChangeType) source.ChangeType).Name(),
                EntityType = ((EntityType) source.EntityType).Name(),
                Id = source.EntityId,
                ProjectId = source.ProjectId,
                SequenceNumber = source.SequenceNumber,
                EventTime = source.EventTime,
                Columns = source.Columns
            };
        }
    }
}