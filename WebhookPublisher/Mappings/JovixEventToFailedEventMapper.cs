﻿using WebhookPublisher.Domain;

namespace WebhookPublisher.Mappings
{
    public class JovixEventToFailedEventMapper
    {
        public virtual FailedEvent Map(JovixEvent jovixEvent, string url)
        {
            return new FailedEvent
            {
                ChangeType = (int) jovixEvent.ChangeType,
                EntityType = (int) jovixEvent.EntityType,
                Columns = jovixEvent.Columns,
                EntityId = jovixEvent.Id,
                ProjectId = jovixEvent.ProjectId,
                EventTime = jovixEvent.Timestamp,
                SequenceNumber = jovixEvent.SequenceNumber,
                Url = url,
                RetryCount = 0
            };
        }
    }
}