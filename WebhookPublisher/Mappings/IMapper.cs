﻿namespace WebhookPublisher.Mappings
{
    public interface IMapper<in TSource, out TResult>
    {
        TResult Map(TSource source);
    }
}