﻿using System.ComponentModel;

namespace WebhookPublisher.Enums
{
    public enum ChangeType
    {
        [Description("Create")] Create = 0,
        [Description("Update")] Update = 1,
        [Description("Delete")] Delete = 2
    }
}