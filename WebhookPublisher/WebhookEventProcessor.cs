﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Microsoft.Azure.EventHubs;
using Microsoft.Azure.EventHubs.Processor;
using Newtonsoft.Json;
using WebhookPublisher.Domain;
using WebhookPublisher.Logging;
using WebhookPublisher.Services;

namespace WebhookPublisher
{
    public class WebhookEventProcessor : IEventProcessor
    {
        private readonly ILog _log;
        private readonly WebhookSubscriptionService _subscriptionService;

        public WebhookEventProcessor()
        {
            _subscriptionService = new WebhookSubscriptionService();
            _log = LoggerFactory.GetLogger();
        }

        public Task CloseAsync(PartitionContext context, CloseReason reason)
        {
            _log.Info($"Processor Shutting Down. Partition '{context.PartitionId}', Reason: '{reason}'.");
            return Task.CompletedTask;
        }

        public Task OpenAsync(PartitionContext context)
        {
            _log.Info($"SimpleEventProcessor initialized. Partition: '{context.PartitionId}'");
            return Task.CompletedTask;
        }

        public Task ProcessErrorAsync(PartitionContext context, Exception error)
        {
            _log.Error($"Error on Partition: {context.PartitionId}, Error: {error.Message}");
            return Task.CompletedTask;
        }

        public async Task ProcessEventsAsync(PartitionContext context, IEnumerable<EventData> messages)
        {
            var eventDatas = messages as EventData[] ?? messages.ToArray();
            _log.Info($"Message Batch Count: {eventDatas.Length}");
            var returnEach = Parallel.ForEach(eventDatas, async e => await ProcessEventAsync(context, e));

            while (!returnEach.IsCompleted)
            {
                await Task.Delay(500);
            }

            await context.CheckpointAsync();
        }

        private async Task ProcessEventAsync(PartitionContext context, EventData eventData)
        {
            var data = Encoding.UTF8.GetString(eventData.Body.Array, eventData.Body.Offset, eventData.Body.Count);
            var jovixEvent = JsonConvert.DeserializeObject<JovixEvent>(data);
            _log.Info($"Message received. Partition: '{context.PartitionId}', Data: '{data}'");
            await _subscriptionService.PostEventAsync(jovixEvent);
        }
    }
}