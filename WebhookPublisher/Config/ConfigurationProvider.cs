﻿using System.IO;
using Microsoft.Extensions.Configuration;

namespace WebhookPublisher.Config
{
    public class ConfigurationProvider
    {
        private readonly IConfiguration _configuration;
        private const string EventHubConnectionStringKey = "connectionStrings:eventHubConnectionString";
        private const string DatabaseConnectionStringKey = "connectionStrings:databaseConnectionString";
        private const string PollingPeriodKey = "retryPollingInterval";
        private const string RetryLimit = "retryLimit";
        private const string StorageContainerName = "storageContainerName";
        private const string BatchSize = "batchSize";
        private const string EntityPath = "eventHubEntityPath";


        public ConfigurationProvider(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public ConfigurationProvider()
        {
            _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
        }

        public virtual string GetEventHubConnectionString()
        {
            return _configuration[EventHubConnectionStringKey];
        }

        public virtual string GetDatabaseConnectionString()
        {
            return _configuration[DatabaseConnectionStringKey];
        }

        public virtual int GetRetryLimit()
        {
            return int.Parse(_configuration[RetryLimit]);
        }

        public virtual int GetRetryDelay()
        {
            return int.Parse(_configuration[PollingPeriodKey]) * 1000;
        }

        public virtual string GetStorageContainerName()
        {
            return _configuration[StorageContainerName];
        }

        public virtual string GetEntityPath()
        {
            return _configuration[EntityPath];
        }

        public virtual int GetBatchSize()
        {
            return int.Parse(_configuration[BatchSize]);
        }
    }
}