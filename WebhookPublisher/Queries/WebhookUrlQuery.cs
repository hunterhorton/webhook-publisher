﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebhookPublisher.Enums;

namespace WebhookPublisher.Queries
{
    public class WebhookUrlQuery
    {
        public virtual async Task<IList<string>> GetUrls(Guid jovixEventInstanceId, int jovixEventProjectId,
            EntityType jovixEventEntityType, ChangeType jovixEventChangeType)
        {
            return await Task.FromResult(new List<string>());
        }
    }
}