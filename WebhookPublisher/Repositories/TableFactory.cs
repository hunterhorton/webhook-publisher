﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using WebhookPublisher.Config;
using WebhookPublisher.Enums;
using WebhookPublisher.Extensions;

namespace WebhookPublisher.Repositories
{
    public class TableFactory
    {
        private readonly ConfigurationProvider _configurationProvider;

        public TableFactory() : this(new ConfigurationProvider())
        {
        }

        public TableFactory(ConfigurationProvider configurationProvider)
        {
            _configurationProvider = configurationProvider;
        }

        public CloudTable GetTable(TableName tableName)
        {
            var account = CloudStorageAccount.Parse(_configurationProvider.GetDatabaseConnectionString());
            return account.CreateCloudTableClient().GetTableReference(tableName.Name());
        }
    }
}