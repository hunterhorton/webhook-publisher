﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Table;
using WebhookPublisher.Domain;
using WebhookPublisher.Enums;

namespace WebhookPublisher.Repositories
{
    public class FailedEventsRepository
    {
        private readonly TableFactory _tableFactory;
        private static readonly Random _random = new Random();

        public FailedEventsRepository() : this(new TableFactory())
        {
        }

        public FailedEventsRepository(TableFactory tableFactory)
        {
            _tableFactory = tableFactory;
        }

        public virtual async Task SaveEventAsync(FailedEvent failedEvent)
        {
            failedEvent.PartitionKey = "1";
            failedEvent.RowKey = _random.Next().ToString();
            failedEvent.ETag = "*";
            await _tableFactory.GetTable(TableName.FailedEvents).ExecuteAsync(TableOperation.Insert(failedEvent));
        }

        public virtual async Task<IList<FailedEvent>> GetFailedEvents()
        {
            return await ExecuteQueryAsync(_tableFactory.GetTable(TableName.FailedEvents), new TableQuery<FailedEvent>());
        }

        public virtual async Task RemoveFailedEvent(FailedEvent failedEvent)
        {
            await _tableFactory.GetTable(TableName.FailedEvents).ExecuteAsync(TableOperation.Delete(failedEvent));
        }

        public virtual async Task UpdateRetryCount(FailedEvent failedEvent)
        {
            await _tableFactory.GetTable(TableName.FailedEvents).ExecuteAsync(TableOperation.Merge(failedEvent));
        }

        private static async Task<IList<T>> ExecuteQueryAsync<T>(CloudTable table, TableQuery<T> query,
            CancellationToken ct = default(CancellationToken), Action<IList<T>> onProgress = null)
            where T : ITableEntity, new()
        {
            var items = new List<T>();
            TableContinuationToken token = null;

            do
            {
                var seg = await table.ExecuteQuerySegmentedAsync(query, token);
                token = seg.ContinuationToken;
                items.AddRange(seg);
                onProgress?.Invoke(items);
            } while (token != null && !ct.IsCancellationRequested);

            return items;
        }
    }
}