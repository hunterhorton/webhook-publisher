﻿using NUnit.Framework;
using WebhookPublisher.Domain;
using WebhookPublisher.Enums;
using WebhookPublisher.Extensions;
using WebhookPublisher.Mappings;

namespace WebhookPublisherTest.Mappings
{
    public class FailedEventToWebhookEventMapper_should_
    {
        [Test]
        public void map_failed_event_to_webhook_event()
        {
            var failedEvent = Any.RandomlyGenerated<FailedEvent>();
            var mapper = new FailedEventToWebhookEventMapper();
            var webhookEvent = mapper.Map(failedEvent);

            Assert.AreEqual(failedEvent.EntityId, webhookEvent.Id);
            Assert.AreEqual(failedEvent.ProjectId, webhookEvent.ProjectId);
            Assert.AreEqual(((ChangeType) failedEvent.ChangeType).Name(), webhookEvent.ChangeType);
            Assert.AreEqual(failedEvent.EventTime, webhookEvent.EventTime);
            Assert.AreEqual(((EntityType) failedEvent.EntityType).Name(), webhookEvent.EntityType);
            Assert.AreEqual(failedEvent.Columns, webhookEvent.Columns);
            Assert.AreEqual(failedEvent.SequenceNumber, webhookEvent.SequenceNumber);
        }
    }
}