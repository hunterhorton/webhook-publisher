﻿using NUnit.Framework;
using WebhookPublisher.Domain;
using WebhookPublisher.Mappings;

namespace WebhookPublisherTest.Mappings
{
    public class JovixEventToFailedEventMapper_should_
    {
        [Test]
        public void map_jovix_event_to_failed_event_mapper()
        {
            var jovixEvent = Any.RandomlyGenerated<JovixEvent>();
            var url = Any.Url();
            var mapper = new JovixEventToFailedEventMapper();
            
            var actual = mapper.Map(jovixEvent, url);
            
            Assert.AreEqual(jovixEvent.Id, actual.EntityId);
            Assert.AreEqual((int)jovixEvent.EntityType, actual.EntityType);
            Assert.AreEqual((int)jovixEvent.ChangeType, actual.ChangeType);
            Assert.AreEqual(jovixEvent.Columns, actual.Columns);
            Assert.AreEqual(jovixEvent.SequenceNumber, actual.SequenceNumber);
            Assert.AreEqual(jovixEvent.Timestamp, actual.EventTime);
            Assert.AreEqual(jovixEvent.ProjectId, actual.ProjectId);
            Assert.AreEqual(url, actual.Url);
            Assert.AreEqual(0, actual.RetryCount);
            
        }
        
    }
}