﻿using NUnit.Framework;
using WebhookPublisher.Domain;
using WebhookPublisher.Extensions;
using WebhookPublisher.Mappings;

namespace WebhookPublisherTest.Mappings
{
    public class JovixEventToWebhookEventMapper_should_
    {
        [Test]
        public void map_jovix_event_to_webhook_event()
        {
            var jovixEvent = Any.RandomlyGenerated<JovixEvent>();
            var mapper = new JovixEventToWebhookEventMapper();
            var webhookEvent = mapper.Map(jovixEvent);
            
            Assert.AreEqual(jovixEvent.ChangeType.Name(), webhookEvent.ChangeType);
            Assert.AreEqual(jovixEvent.EntityType.Name(), webhookEvent.EntityType);
            Assert.AreEqual(jovixEvent.ProjectId, webhookEvent.ProjectId);
            Assert.AreEqual(jovixEvent.SequenceNumber, webhookEvent.SequenceNumber);
            Assert.AreEqual(jovixEvent.Timestamp, webhookEvent.EventTime);
            Assert.AreEqual(jovixEvent.Id, webhookEvent.Id);
            Assert.AreEqual(jovixEvent.Columns, webhookEvent.Columns);
        }
    }
}