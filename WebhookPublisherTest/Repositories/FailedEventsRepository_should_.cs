﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.WindowsAzure.Storage.Table;
using Moq;
using NUnit.Framework;
using WebhookPublisher.Config;
using WebhookPublisher.Domain;
using WebhookPublisher.Enums;
using WebhookPublisher.Repositories;

namespace WebhookPublisherTest.Repositories
{
    public class FailedEventsRepository_should_
    {
        private static async Task AddFailedEvent(ITableEntity failedEvent)
        {
            await new TableFactory(GetConfigProvider()).GetTable(TableName.FailedEvents)
                .ExecuteAsync(TableOperation.Insert(failedEvent));
        }

        private static async Task AddFailedEvents(IEnumerable<ITableEntity> failedEvents)
        {
            var ops = failedEvents.Select(TableOperation.Insert).ToList();
            var batch = new TableBatchOperation();
            ops.ForEach(op => batch.Add(op));
            await new TableFactory(GetConfigProvider()).GetTable(TableName.FailedEvents)
                .ExecuteBatchAsync(batch);
        }

        private static async Task RemoveFailedEvent(ITableEntity failedEvent)
        {
            var table = new TableFactory(GetConfigProvider()).GetTable(TableName.FailedEvents);
            await table.ExecuteAsync(TableOperation.Delete(failedEvent));
        }

        private static async Task RemoveFailedEvents(IEnumerable<ITableEntity> failedEvents)
        {
            var ops = failedEvents.Select(TableOperation.Delete).ToList();
            var batch = new TableBatchOperation();
            ops.ForEach(op => batch.Add(op));
            var table = new TableFactory(GetConfigProvider()).GetTable(TableName.FailedEvents);
            await table.ExecuteBatchAsync(batch);
        }

        [Test]
        public async Task save_event_to_table_storage()
        {
            var repository = new FailedEventsRepository(new TableFactory(GetConfigProvider()));
            var failedEvent = Any.RandomlyGenerated<FailedEvent>();
            failedEvent.EventTime = Any.PastDate();
            await repository.SaveEventAsync(failedEvent);
            var table = new TableFactory(GetConfigProvider()).GetTable(TableName.FailedEvents);

            var actual =
                await table
                    .ExecuteAsync(TableOperation.Retrieve<FailedEvent>(failedEvent.PartitionKey, failedEvent.RowKey));

            await RemoveFailedEvent(failedEvent);

            failedEvent.Should().BeEquivalentTo(actual.Result as FailedEvent);
        }

        [Test]
        public async Task remove_event_from_table_storage()
        {
            var failedEvent = Any.RandomlyGenerated<FailedEvent>();
            SetupFailedEvent(failedEvent);
            await AddFailedEvent(failedEvent);
            var repository = new FailedEventsRepository(new TableFactory(GetConfigProvider()));
            await repository.RemoveFailedEvent(failedEvent);

            var table = new TableFactory(GetConfigProvider()).GetTable(TableName.FailedEvents);
            var actual =
                await table
                    .ExecuteAsync(TableOperation.Retrieve<FailedEvent>(failedEvent.PartitionKey, failedEvent.RowKey));
            Assert.IsNull(actual.Result);
        }

        [Test]
        public async Task update_event_in_table_storage()
        {
            var failedEvent = Any.RandomlyGenerated<FailedEvent>();
            SetupFailedEvent(failedEvent);
            await AddFailedEvent(failedEvent);
            failedEvent.RetryCount++;
            var repository = new FailedEventsRepository(new TableFactory(GetConfigProvider()));
            await repository.UpdateRetryCount(failedEvent);

            var table = new TableFactory(GetConfigProvider()).GetTable(TableName.FailedEvents);
            var actual = table
                .ExecuteAsync(TableOperation.Retrieve<FailedEvent>(failedEvent.PartitionKey, failedEvent.RowKey))
                .Result;

            await RemoveFailedEvent(failedEvent);

            Assert.AreEqual(failedEvent.RetryCount, ((FailedEvent) actual.Result).RetryCount);
        }

        [Test]
        public async Task return_failed_events()
        {
            var failedEvents = SetupFailedEvents();
            await AddFailedEvents(failedEvents);
            var repository = new FailedEventsRepository(new TableFactory(GetConfigProvider()));

            var actual = await repository.GetFailedEvents();

            await RemoveFailedEvents(failedEvents);

            actual.Should().BeEquivalentTo(failedEvents);
        }

        private static List<FailedEvent> SetupFailedEvents()
        {
            var failedEvents = Any.ListOfType(Any.RandomlyGenerated<FailedEvent>, 1, 5);
            foreach (var t in failedEvents)
            {
                SetupFailedEvent(t);
            }

            return failedEvents;
        }

        private static void SetupFailedEvent(FailedEvent t)
        {
            t.PartitionKey = "1";
            t.RowKey = Math.Abs(Any.Int()).ToString();
            t.ETag = "*";
            t.EventTime = Any.PastDate();
        }

        private static ConfigurationProvider GetConfigProvider()
        {
            var configStub = new Mock<ConfigurationProvider>(null);
            configStub.Setup(c => c.GetDatabaseConnectionString()).Returns(
                "DefaultEndpointsProtocol=https;AccountName=jovixeventsdatadev001;AccountKey=sUybuz+pETMhm6+8/TJId2BrjMH/F6wovovLLVhT6ppWgOE54nBg59xGs3+jpV2kiYp0EGc3KmwCqVu8RD3cDQ==");
            return configStub.Object;
        }
    }
}