﻿using Moq;
using NUnit.Framework;
using WebhookPublisher.Config;
using WebhookPublisher.Enums;
using WebhookPublisher.Extensions;
using WebhookPublisher.Repositories;

namespace WebhookPublisherTest.Repositories
{
    public class TableFactory_should
    {
        [Test]
        public void get_reference_to_table()
        {
            var configStub = new Mock<ConfigurationProvider>(null);
            configStub.Setup(c => c.GetDatabaseConnectionString()).Returns(
                "DefaultEndpointsProtocol=https;AccountName=jovixeventsdatadev001;AccountKey=sUybuz+pETMhm6+8/TJId2BrjMH/F6wovovLLVhT6ppWgOE54nBg59xGs3+jpV2kiYp0EGc3KmwCqVu8RD3cDQ==");
            var factory = new TableFactory(configStub.Object);

            var table = factory.GetTable(TableName.FailedEvents);

            Assert.AreEqual(TableName.FailedEvents.Name(), table.Name);
        }
    }
}