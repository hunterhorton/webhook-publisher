﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using WebhookPublisher.Domain;
using WebhookPublisher.Mappings;
using WebhookPublisher.Queries;
using WebhookPublisher.Repositories;
using WebhookPublisher.Services;

namespace WebhookPublisherTest.Services
{
    public class WebhookSubscriptionService_should_
    {
        [Test]
        public async Task get_subscriptions_from_db()
        {
            var jovixEvent = Any.RandomlyGenerated<JovixEvent>();
            var expectedUrls = (IList<string>) Any.ListOfType(Any.AlphanumericString);
            var webhookUrlQueryMock = new Mock<WebhookUrlQuery>();
            webhookUrlQueryMock
                .Setup(q => q.GetUrls(jovixEvent.InstanceId, jovixEvent.ProjectId, jovixEvent.EntityType,
                    jovixEvent.ChangeType)).Returns(Task.FromResult(expectedUrls));
            var service =
                new WebhookSubscriptionService(webhookUrlQueryMock.Object, new Mock<WebhookUrlPoster>().Object,
                    new Mock<FailedEventsRepository>(null).Object, new JovixEventToWebhookEventMapper(), new JovixEventToFailedEventMapper());

            await service.PostEventAsync(jovixEvent);

            webhookUrlQueryMock.Verify(q => q.GetUrls(jovixEvent.InstanceId,
                jovixEvent.ProjectId,
                jovixEvent.EntityType,
                jovixEvent.ChangeType), Times.Once);
        }

        [Test]
        public async Task send_the_event_to_the_urls()
        {
            var jovixEvent = Any.RandomlyGenerated<JovixEvent>();
            var webhookEvent = Any.RandomlyGenerated<WebhookEvent>();
            var expectedUrls = (IList<string>) Any.ListOfType(Any.AlphanumericString);
            var webhookUrlQueryMock = new Mock<WebhookUrlQuery>();
            webhookUrlQueryMock
                .Setup(q => q.GetUrls(jovixEvent.InstanceId, jovixEvent.ProjectId, jovixEvent.EntityType,
                    jovixEvent.ChangeType)).Returns(Task.FromResult(expectedUrls));
            var webhookUrlPosterMock = new Mock<WebhookUrlPoster>();
            var mapper = new Mock<JovixEventToWebhookEventMapper>();
            mapper.Setup(m => m.Map(jovixEvent)).Returns(webhookEvent);

            var service = new WebhookSubscriptionService(webhookUrlQueryMock.Object, webhookUrlPosterMock.Object,
                new Mock<FailedEventsRepository>(null).Object, mapper.Object, new JovixEventToFailedEventMapper());

            await service.PostEventAsync(jovixEvent);

            webhookUrlPosterMock.Verify(p => p.PostAsync(webhookEvent, It.IsAny<string>()), Times.Exactly(expectedUrls.Count));
        }

        [Test]
        public async Task save_messages_that_do_not_post()
        {
            var jovixEvent = Any.RandomlyGenerated<JovixEvent>();
            var expectedUrls = (IList<string>) Any.ListOfType(Any.AlphanumericString);
            var failedEvent = Any.RandomlyGenerated<FailedEvent>();
            var webhookUrlQueryMock = new Mock<WebhookUrlQuery>();
            webhookUrlQueryMock
                .Setup(q => q.GetUrls(jovixEvent.InstanceId, jovixEvent.ProjectId, jovixEvent.EntityType,
                    jovixEvent.ChangeType)).Returns(Task.FromResult(expectedUrls));
            var webhookUrlPosterMock = new Mock<WebhookUrlPoster>();
            webhookUrlPosterMock.Setup(p => p.PostAsync(It.IsAny<WebhookEvent>(), It.IsAny<string>())).Throws<HttpRequestException>();
            var failedEventsRepo = new Mock<FailedEventsRepository>(null);
            var mapper = new Mock<JovixEventToFailedEventMapper>();
            mapper.Setup(m => m.Map(jovixEvent, It.Is<string>(u => expectedUrls.Contains(u)))).Returns(failedEvent);

            var service = new WebhookSubscriptionService(webhookUrlQueryMock.Object, webhookUrlPosterMock.Object,
                failedEventsRepo.Object, new JovixEventToWebhookEventMapper(), mapper.Object);

            await service.PostEventAsync(jovixEvent);

            failedEventsRepo.Verify(e => e.SaveEventAsync(failedEvent), Times.Exactly(expectedUrls.Count));
        }
    }
}