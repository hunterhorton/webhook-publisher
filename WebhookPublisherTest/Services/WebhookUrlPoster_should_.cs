﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using WebhookPublisher;
using WebhookPublisher.Domain;
using WebhookPublisher.Services;

namespace WebhookPublisherTest.Services
{
    public class WebhookUrlPoster_should_
    {
        [Test]
        public async Task post_event_to_url()
        {
            var expectedEvent = Any.RandomlyGenerated<WebhookEvent>();
            var expectedBody = JsonConvert.SerializeObject(expectedEvent);
            var url = Any.Url();
            var response = Any.RandomlyGenerated<HttpResponseMessage>();
            response.StatusCode = HttpStatusCode.OK;

            var httpClientWrapper = new Mock<HttpClientWrapper>();
            httpClientWrapper.Setup(w => w.SendAsync(It.IsAny<HttpRequestMessage>())).ReturnsAsync(response);

            var webhookUrlPoster = new WebhookUrlPoster(httpClientWrapper.Object);

            await webhookUrlPoster.PostAsync(expectedEvent, url);

            httpClientWrapper.Verify(w => w.SendAsync(
                It.Is<HttpRequestMessage>(m => m.RequestUri.OriginalString == url
                                               && m.Content.ReadAsStringAsync().Result == expectedBody)));
        }

        [TestCase(HttpStatusCode.OK)]
        [TestCase(HttpStatusCode.Accepted)]
        public async Task not_throw_if_post_was_successful(HttpStatusCode code)
        {
            var wrapper = new Mock<HttpClientWrapper>();
            wrapper.Setup(w => w.SendAsync(It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(new HttpResponseMessage(code)));

            var webhookPoster = new WebhookUrlPoster(wrapper.Object);

            Assert.DoesNotThrow(() =>
            {
                var task = webhookPoster.PostAsync(Any.RandomlyGenerated<WebhookEvent>(), Any.Url());
                task.Wait();
            });
        }

        [TestCase(HttpStatusCode.NotFound)]
        [TestCase(HttpStatusCode.BadRequest)]
        public async Task throw_if_post_was_unsuccessful(HttpStatusCode code)
        {
            var wrapper = new Mock<HttpClientWrapper>();
            wrapper.Setup(w => w.SendAsync(It.IsAny<HttpRequestMessage>()))
                .Returns(Task.FromResult(new HttpResponseMessage(code)));

            var webhookPoster = new WebhookUrlPoster(wrapper.Object);

            Assert.Throws<HttpRequestException>(() =>
            {
                var task = webhookPoster.PostAsync(Any.RandomlyGenerated<WebhookEvent>(), Any.Url());
                try
                {
                    task.Wait();
                }
                catch (AggregateException e)
                {
                    throw e.InnerException;
                }
            });
        }
    }
}