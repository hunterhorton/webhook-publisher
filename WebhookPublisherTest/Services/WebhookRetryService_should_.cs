﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using WebhookPublisher.Config;
using WebhookPublisher.Domain;
using WebhookPublisher.Mappings;
using WebhookPublisher.Repositories;
using WebhookPublisher.Services;

namespace WebhookPublisherTest.Services
{
    public class WebhookRetryService_should_
    {
        [Test]
        public async Task get_failed_events_from_database()
        {
            var repository = new Mock<FailedEventsRepository>(null);
            var events = (IList<FailedEvent>) Any.ListOfType(Any.RandomlyGenerated<FailedEvent>);
            repository.Setup(q => q.GetFailedEvents()).Returns(Task.FromResult(events));
            var service = new WebhookRetryService(repository.Object, new Mock<WebhookUrlPoster>().Object,
                new FailedEventToWebhookEventMapper(), new Mock<ConfigurationProvider>(null).Object);

            await service.Retry();

            repository.Verify(q => q.GetFailedEvents(), Times.Once);
        }

        [Test]
        public async Task post_failed_events_to_url()
        {
            var repository = new Mock<FailedEventsRepository>(null);
            var events = (IList<FailedEvent>) Any.ListOfType(Any.RandomlyGenerated<FailedEvent>);
            repository.Setup(q => q.GetFailedEvents()).Returns(Task.FromResult(events));
            var poster = new Mock<WebhookUrlPoster>();
            var service = new WebhookRetryService(repository.Object, poster.Object,
                new FailedEventToWebhookEventMapper(), new Mock<ConfigurationProvider>(null).Object);

            await service.Retry();

            poster.Verify(p =>
                    p.PostAsync(It.Is<WebhookEvent>(w => events.Any(e => e.EntityId == w.Id)),
                        It.Is<string>(u => events.Any(e => e.Url == u))),
                Times.Exactly(events.Count));
        }

        [Test]
        public async Task remove_failed_event_if_post_successful()
        {
            var failedEvent = Any.RandomlyGenerated<FailedEvent>();
            var repository = new Mock<FailedEventsRepository>(null);
            repository.Setup(q => q.GetFailedEvents())
                .ReturnsAsync(new List<FailedEvent> { failedEvent });
            var poster = new Mock<WebhookUrlPoster>();

            var service = new WebhookRetryService(repository.Object, poster.Object,
                new Mock<FailedEventToWebhookEventMapper>().Object, new Mock<ConfigurationProvider>(null).Object);

            await service.Retry();

            repository.Verify(c => c.RemoveFailedEvent(failedEvent), Times.Once);
        }

        [TestCase(5)]
        [TestCase(4)]
        public async Task remove_failed_event_if_exceeds_or_equals_retry_limit(int retryCount)
        {
            var failedEvent = Any.RandomlyGenerated<FailedEvent>();
            failedEvent.RetryCount = retryCount;
            var repository = new Mock<FailedEventsRepository>(null);
            repository.Setup(q => q.GetFailedEvents())
                .ReturnsAsync(new List<FailedEvent> { failedEvent });
            var poster = new Mock<WebhookUrlPoster>();
            poster.Setup(p => p.PostAsync(It.IsAny<WebhookEvent>(), It.IsAny<string>())).Throws<HttpRequestException>();
            var configStub = new Mock<ConfigurationProvider>(null);
            configStub.Setup(c => c.GetRetryLimit()).Returns(5);

            var service = new WebhookRetryService(repository.Object, poster.Object,
                new Mock<FailedEventToWebhookEventMapper>().Object, configStub.Object);

            await service.Retry();

            repository.Verify(c => c.RemoveFailedEvent(failedEvent), Times.Once);
        }

        [Test]
        public async Task increment_retry_count_when_the_post_fails()
        {
            var expectedRetryCount = Any.Int();
            var failedEvent = Any.RandomlyGenerated<FailedEvent>();
            failedEvent.RetryCount = expectedRetryCount;
            var repository = new Mock<FailedEventsRepository>(null);
            repository.Setup(q => q.GetFailedEvents())
                .ReturnsAsync(new List<FailedEvent> { failedEvent });
            var poster = new Mock<WebhookUrlPoster>();
            poster.Setup(r => r.PostAsync(It.IsAny<WebhookEvent>(), It.IsAny<string>()))
                .Throws<HttpRequestException>();
            var configStub = new Mock<ConfigurationProvider>(null);
            configStub.Setup(c => c.GetRetryLimit()).Returns(Any.IntGreaterThanOrEqualTo(failedEvent.RetryCount) + 1);

            var service = new WebhookRetryService(repository.Object, poster.Object,
                new Mock<FailedEventToWebhookEventMapper>().Object, configStub.Object);

            await service.Retry();

            repository.Verify(c => c.UpdateRetryCount(It.Is<FailedEvent>(e => e.RetryCount == expectedRetryCount + 1)),
                Times.Once);
        }
    }
}