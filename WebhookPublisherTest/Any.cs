﻿using System;
using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using FizzWare.NBuilder.PropertyNaming;
using WebhookPublisher.Extensions;

namespace WebhookPublisherTest
{
    public class Any
    {
        private static readonly Random Random = new Random();
        private const int ArbitrarilyHighInteger = 32;
        private const string Alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private const string AlphanumericCharacters = Alphabet + Numbers;
        private const string Base64 = Alphabet + Numbers + "+/";
        private const string Hex = Numbers + "ABCDEF";
        private const string Numbers = "0123456789";
        private const string Symbols = "`-=[]\\;',./~!@#$%^&*()_+{}|:\"<>?";

        public static bool Bool()
        {
            return Convert.ToBoolean(IntBetween(0, 1));
        }

        public static byte[] ByteArray(int length = 256)
        {
            var randomBytes = new byte[length];
            Random.NextBytes(randomBytes);
            return randomBytes;
        }

        public static T[] CollectionOfType<T>(Func<T> creationFunction, int minSize = 1, int maxSize = 16)
        {
            var collection = new T[IntBetween(minSize, maxSize)];

            foreach (var index in Enumerable.Range(0, collection.Length))
            {
                collection[index] = creationFunction();
            }

            return collection;
        }

        public static List<T> ListOfType<T>(Func<T> creationFunction, int minSize = 1, int maxSize = 16)
        {
            var list = new List<T>();
            var listSize = IntBetween(minSize, maxSize);

            for (var i = 0; i < listSize; i++)
            {
                list.Add(creationFunction());
            }

            return list;
        }

        public static DateTime Date()
        {
            return DateTime.UtcNow;
        }

        public static decimal Decimal(double min = 1, double max = 100)
        {
            return (decimal) (Random.NextDouble() * (max - min) + min);
        }

        public static double Double(int min = 1, int max = 100)
        {
            return Random.NextDouble() * (max - min) + min;
        }

        public static TEnum EnumValue<TEnum>() where TEnum : struct, IConvertible, IFormattable
        {
            var values = ((TEnum[]) Enum.GetValues(typeof(TEnum))).ToList();

            var random = new Random();

            return values[random.Next(values.Count)];
        }

        public static TEnum EnumValueWithSkips<TEnum>(params TEnum[] skips)
            where TEnum : struct, IConvertible, IFormattable
        {
            var values = ((TEnum[]) Enum.GetValues(typeof(TEnum))).ToList();

            values.RemoveAll(skips.Contains);

            var random = new Random();

            return values[random.Next(values.Count)];
        }

        public static DateTime FutureDate()
        {
            return DateTime.UtcNow.Add(TimeSpan.FromHours(IntBetween(1, 1000)));
        }

        public static Guid Guid()
        {
            return System.Guid.NewGuid();
        }

        public static IEnumerable<Guid> Guids(int min = 0, int max = 10)
        {
            return CollectionOfType(Guid, min, max);
        }

        public static int Int()
        {
            return Random.Next();
        }

        public static int IntBetween(int min, int max)
        {
            var sizeOfInclusiveRange = max - min + 1;
            return Random.Next() % sizeOfInclusiveRange + min;
        }

        public static int IntExcept(int excluded)
        {
            var integer = Int();

            while (integer == excluded)
            {
                integer = Int();
            }

            return integer;
        }

        public static int IntGreaterThanOrEqualTo(int min)
        {
            return IntBetween(min, Int());
        }

        public static decimal? NullableDecimal()
        {
            if (Bool())
            {
                return null;
            }

            return Decimal();
        }

        public static double? NullableDouble()
        {
            if (Bool())
            {
                return null;
            }

            return Double();
        }

        public static Guid? NullableGuid()
        {
            return Guid();
        }

        public static object Object()
        {
            return new object();
        }

        public static object[] Objects(int min = 1, int max = 16)
        {
            return CollectionOfType(Object, min, max);
        }

        public static DateTime PastDate()
        {
            return DateTime.UtcNow.Subtract(TimeSpan.FromHours(IntBetween(1, 1000)));
        }

        public static T RandomlyGenerated<T>()
        {
            BuilderSetup.SetDefaultPropertyName(new RandomValuePropertyNamer(new BuilderSettings()));
            return Builder<T>.CreateNew().Build();
        }

        public static T RandomValueFromEnum<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>().Random();
        }

        public static int SequenceNumber(int min, int max)
        {
            return IntBetween(min, max);
        }

        public static object Short()
        {
            return (short) (Int() % short.MaxValue);
        }

        public static long Long()
        {
            return ((long) Int() + Int()) % long.MaxValue;
        }

        public static string AlphabeticString(int min = 1, int max = ArbitrarilyHighInteger)
        {
            var length = IntBetween(min, max);
            return RandomString(length, Alphabet);
        }

        public static string AlphabeticStringExcept(params string[] stringsToExclude)
        {
            return AlphabeticStringExcept(1, ArbitrarilyHighInteger, stringsToExclude);
        }

        public static string AlphabeticStringExcept(int min, int max, params string[] stringsToExclude)
        {
            return AlphabeticStringExcept(StringComparer.OrdinalIgnoreCase, min, max, stringsToExclude);
        }

        public static string AlphabeticStringExcept(IEqualityComparer<string> comparer, int min, int max,
            params string[] stringsToExclude)
        {
            string alphabeticString;
            do
            {
                alphabeticString = AlphabeticString(min, max);
            } while (stringsToExclude.Contains(alphabeticString, comparer));

            return alphabeticString;
        }

        public static string AlphanumericString(int length = -1)
        {
            if (length < 0)
            {
                length = IntBetween(5, ArbitrarilyHighInteger);
            }

            var stringChars = Enumerable.Repeat(AlphanumericCharacters, length).Select(chars => chars.Random());
            return new string(stringChars.ToArray());
        }

        public static string AlphanumericString()
        {
            var length = -1;
            if (length < 0)
            {
                length = IntBetween(5, ArbitrarilyHighInteger);
            }

            var stringChars = Enumerable.Repeat(AlphanumericCharacters, length).Select(chars => chars.Random());
            return new string(stringChars.ToArray());
        }

        public static string AlphanumericString(int min, int max)
        {
            return AlphanumericString(IntBetween(min, max));
        }

        public static string Base64String(int min = 1, int max = 16)
        {
            return RandomString(IntBetween(min, max), Base64);
        }

        public static string HexString(int min = 1, int max = 16)
        {
            return RandomString(IntBetween(min, max), Hex);
        }

        public static string NumericString(int min = 1, int max = 16)
        {
            return RandomString(IntBetween(min, max), Numbers);
        }

        public static string[] Strings(int minSize = 1)
        {
            return CollectionOfType(() => AlphanumericString(), minSize);
        }

        public static string StringWithSymbols(int min = 1, int max = 16)
        {
            return RandomString(IntBetween(min, max), Alphabet + Numbers + Symbols);
        }

        private static string RandomString(int length, string allowedCharacters = Alphabet + Numbers)
        {
            return new string(Enumerable.Repeat(allowedCharacters, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }

        public static string Url()
        {
            return $"http://www.{AlphanumericString()}.com";
        }
    }
}