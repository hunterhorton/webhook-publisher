﻿using NUnit.Framework;
using WebhookPublisher.Logging;

namespace WebhookPublisherTest
{
    [SetUpFixture]
    public class Setup
    {
        [OneTimeSetUp]
        public void setup()
        {
            LoggerFactory.SetupLogger();
        }
    }
}