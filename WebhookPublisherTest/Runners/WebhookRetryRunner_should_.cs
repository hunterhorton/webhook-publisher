﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using WebhookPublisher.Config;
using WebhookPublisher.Runners;
using WebhookPublisher.Services;

namespace WebhookPublisherTest.Runners
{
    public class WebhookRetryRunner_should_
    {
        [Test]
        public async Task attempt_to_retry_failed_posts()
        {
            var retryServiceMock = new Mock<WebhookRetryService>(null, null, null, null);
            var configStub = new Mock<ConfigurationProvider>(null);
            configStub.Setup(c => c.GetRetryDelay()).Returns(60000);
            var runner = new WebhookRetryRunner(retryServiceMock.Object, configStub.Object);
            var tokenSource = new CancellationTokenSource();

            var runTask = runner.Run(tokenSource.Token);
            var delayTask = Task.Delay(TimeSpan.FromSeconds(1), tokenSource.Token);
            tokenSource.Cancel();
            try
            {
                await Task.WhenAll(delayTask, runTask);
            }
            catch (Exception)
            {
                retryServiceMock.Verify(r => r.Retry(), Times.Once);
            }
        }

        [Test]
        public async Task not_run_if_task_is_cancelled()
        {
            var retryServiceMock = new Mock<WebhookRetryService>(null, null, null, null);
            var configStub = new Mock<ConfigurationProvider>(null);
            configStub.Setup(c => c.GetRetryDelay()).Returns(60000);
            var runner = new WebhookRetryRunner(retryServiceMock.Object, configStub.Object);
            var tokenSource = new CancellationTokenSource();
            tokenSource.Cancel();

            var runTask = runner.Run(tokenSource.Token);
            var delayTask = Task.Delay(TimeSpan.FromSeconds(1), tokenSource.Token);
            try
            {
                await Task.WhenAll(delayTask, runTask);
            }
            catch (Exception)
            {
                retryServiceMock.Verify(r => r.Retry(), Times.Never);
            }
        }
    }
}