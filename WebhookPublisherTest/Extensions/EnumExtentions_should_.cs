﻿using NUnit.Framework;
using WebhookPublisher.Extensions;

namespace WebhookPublisherTest.Extensions
{
    public class EnumExtentions_should_
    {
        [Test]
        public void use_enum_name_when_description_is_null()
        {
            const string expectedName = "NullDescription";
            var actualName = TestEnum.NullDescription.Name();

            Assert.AreEqual(expectedName, actualName);
        }

        [Test]
        public void be_empty_string_when_description_given_no_args()
        {
            const string expectedName = "";
            var actualName = TestEnum.DescriptionWithNoArg.Name();

            Assert.AreEqual(expectedName, actualName);
        }

        [Test]
        public void allow_empty_strings()
        {
            const string expectedName = "";
            var actualName = TestEnum.EmptyStringDescription.Name();

            Assert.AreEqual(expectedName, actualName);
        }

        [Test]
        public void use_enum_name_when_description_attribute_not_used()
        {
            const string expectedName = "NoDescription";
            var actualName = TestEnum.NoDescription.Name();

            Assert.AreEqual(expectedName, actualName);
        }
    }

    public enum TestEnum
    {
        [System.ComponentModel.Description(null)]
        NullDescription,

        [System.ComponentModel.Description("")]
        EmptyStringDescription,
        [System.ComponentModel.Description] DescriptionWithNoArg,
        NoDescription
    }
}